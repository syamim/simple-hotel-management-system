package HotelManagement;

import java.util.Scanner;

public class Accomodation{
  String RoomType,RoomCheckin,RoomCheckout,RoomAddOn,RoomService;
  Integer RoomVol,RoomAddQty,RoomDuration,RoomPeople = 0;
  Double RoomRate,totalOverall,RoomLaundry;
  Scanner console = new Scanner(System.in);  
  
  public Accomodation main(){
    Accomodation temp_accomodation = new Accomodation();
    temp_accomodation.setRoomType();
    temp_accomodation.setRoomVol();
    temp_accomodation.setRoomDuration();
    temp_accomodation.setRoomAddOn();
    temp_accomodation.setRoomService();
    temp_accomodation.calculateRate();
    temp_accomodation.calculateOther();
    temp_accomodation.calculateTotal();
    return temp_accomodation;
  }

  public void setRoomType(){
    System.out.println("   1. Deluxe || 2. Twin Double || 3. Studio || 4. Suite ");
    System.out.println("***********************************************************");
    System.out.print("Enter room type [ex - Deluxe]: ");
    this.RoomType = console.nextLine();
  }
  
  public void setRoomAddOn(){
    System.out.println("   1. Matress || 2. Breakfast || 3. Laundry");
    System.out.println("*************************************************");
    System.out.print("Enter room type [ex - Matress]: ");
    this.RoomAddOn = console.nextLine();
    if (this.RoomAddOn.equals("Breakfast")){
      System.out.print("How many people that will have the breakfast : ");
      this.RoomPeople = console.nextInt();
    }else if (this.RoomAddOn.equals("Laundry")){
      System.out.print("Enter the weight of your laundry : ");
      this.RoomLaundry = console.nextDouble();
    }
  }
  
  public void setRoomService(){
    System.out.println("   1. Jacuzzi || 2. Spa || 3. Massage");
    System.out.println("********************************************");
    System.out.print("Enter room type [ex - Jacuzzi]: ");
    this.RoomAddOn = console.nextLine();
    if (this.RoomPeople == 0){
      System.out.print("How many people that will is with you : ");
      this.RoomPeople = console.nextInt();
    }
  }

  public void setRoomVol(){
    System.out.print("How many room that you will need : ");
    this.RoomVol = Integer.parseInt(console.nextLine());
  }

  public void setRoomDuration(){
    System.out.print("How many night that you will stay : ");
    this.RoomDuration = Integer.parseInt(console.nextLine());
  }
  
  public void setRoomCheckin(){
    System.out.print("Enter Check-in Date [21-01-2020] : ");
    this.RoomCheckin = console.nextLine();
  }
  
  public void setRoomCheckout(){
    System.out.print("Enter Check-out Date [21-01-2020] : ");
    this.RoomCheckout = console.nextLine();
  }

  
  public void calculateRate(){
    if (this.RoomType.equals("Deluxe")){
      this.RoomRate = 100.00 * this.RoomVol;
    }else if(this.RoomType.equals("Twin Double")){
      this.RoomRate = 200.00 * this.RoomVol;
    }else if(this.RoomType.equals("Studio")){
      this.RoomRate = 300.00 * this.RoomVol;
    }else if(this.RoomType.equals("Suite")){
      this.RoomRate = 500.00 * this.RoomVol;
    }
    this.RoomRate = this.RoomRate * this.RoomDuration;
  }

  public void calculateOther(){
    if (this.RoomAddOn.equals("Matress")){
      this.RoomRate = this.RoomRate + (50 * this.RoomDuration);
    }else if(this.RoomAddOn.equals("Breakfast")){
      this.RoomRate = this.RoomRate + (50 * this.RoomPeople);
    }else if(this.RoomAddOn.equals("Laundry")){
      this.RoomRate = this.RoomRate + (15 * this.RoomPeople);
    }

    if (this.RoomService.equals("Jacuzzi")){
      this.RoomRate = this.RoomRate + (20 * this.RoomPeople);
    }else if (this.RoomService.equals("Spa")){
      this.RoomRate = this.RoomRate + (30 * this.RoomPeople);
    }else if (this.RoomService.equals("Massage")){
      this.RoomRate = this.RoomRate + (100 * this.RoomPeople);
    } 
  }

  public void calculateTotal(){
    this.totalOverall = this.RoomRate + (6/100*this.RoomRate);
  }
  
}