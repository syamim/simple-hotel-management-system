package HotelManagement;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Scanner;

public class Booking {
  String CustomerName, CustomerPhoneNo, CustomerEmail, CustomerAddress;
  Training trainingObj = new Training();
  Accomodation accomodationObj = new Accomodation();
  Scanner console = new Scanner(System.in);

  public static void main(String[] args) {
    Booking temp_booking = new Booking();
    temp_booking.display();
  }

  public void display() {
    Table table = new Table();
    System.out.println("   Welcome To Hotel Booking Management");
    System.out.println("==========================================");
    Boolean isNotPassed = true;
    Integer choice = 0;
    do {
      System.out.println("  Select room type that you want to book");
      System.out.println("  1. Accomodation Room || 2. Training Room || 3. View List Accomodation || 4. View List Training ");
      System.out.println("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
      System.out.print("Enter your choice : ");
      choice = Integer.parseInt(console.nextLine());
      if (choice == 1 || choice == 2 || choice == 3 || choice == 4) {
        isNotPassed = false;
      }
    } while (isNotPassed == true);
    if (choice == 2) {
      setCustomerName();
      setCustomerAddress();
      setCustomerPhoneNo();
      setCustomerEmail();
      Training temp_training = new Training();
      this.trainingObj = temp_training.main();
      try {
        reportTraining();
      } catch (IOException e) {
        e.printStackTrace();
      }
    } else if (choice == 1) {
      setCustomerName();
      setCustomerAddress();
      setCustomerPhoneNo();
      setCustomerEmail();
      Accomodation temp_accomodation = new Accomodation();
      this.accomodationObj = temp_accomodation.main();
      try {
        reportAccomodation();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }else if (choice == 3){
      table.printTable("Accomodation.csv");
    }else if (choice == 4){
      table.printTable("Training.csv");
    }
  }

  public void setCustomerName() {
    System.out.print("Enter your name : ");
    this.CustomerName = console.nextLine();
  }

  public void setCustomerPhoneNo() {
    System.out.print("Enter your phone number : ");
    this.CustomerPhoneNo = console.nextLine();
  }

  public void setCustomerAddress() {
    System.out.print("Enter your address : ");
    this.CustomerAddress = console.nextLine();
  }

  public void setCustomerEmail() {
    System.out.print("Enter your email address : ");
    this.CustomerEmail = console.nextLine();
  }

  public String getCustomerName() {
    return this.CustomerName;
  }

  public String getCustomerPhoneNo() {
    return this.CustomerPhoneNo;
  }

  public String getCustomerAddress() {
    return this.CustomerAddress;
  }

  public String getCustomerEmail() {
    return this.CustomerEmail;
  }

  public void reportTraining() throws IOException {
    FileWriter path = new FileWriter(System.getProperty("user.dir") + "/Training.csv",true);
    PrintWriter training_list = new PrintWriter(path);
    training_list.println(this.CustomerName + "," + this.CustomerAddress + "," + this.CustomerPhoneNo + "," + this.CustomerEmail + "," + this.trainingObj.RoomType + ",RM " + this.trainingObj.totalOverall + "," + this.trainingObj.RoomDuration);
    training_list.close();
  }
  
  public void reportAccomodation() throws IOException {
    FileWriter path = new FileWriter(System.getProperty("user.dir") + "/Accomodation.csv",true);
    PrintWriter accomodation_list = new PrintWriter(path);
    accomodation_list.println(this.CustomerName + "," + this.CustomerAddress + "," + this.CustomerPhoneNo + "," + this.CustomerEmail + "," + this.accomodationObj.RoomType + ",RM " + this.accomodationObj.totalOverall + "," + this.accomodationObj.RoomCheckin);
    accomodation_list.close();
  }
}