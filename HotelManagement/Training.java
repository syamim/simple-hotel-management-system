package HotelManagement;

import java.util.Scanner;

public class Training{
  String RoomType,RoomAddEquip,RoomDurationString;
  Integer RoomDuration;
  Double RoomRate,RoomAddRate,RoomOptionMeal,totalOverall;
  Scanner console = new Scanner(System.in);  
  
  public Training main(){
    Training temp_training = new Training();
    temp_training.setRoomType();
    temp_training.setRoomAddEquip();
    temp_training.setRoomOptionMeal();
    temp_training.setRoomDuration();
    temp_training.calculateRate();
    temp_training.calculateOther();
    temp_training.calculateTotal();
    return temp_training;
  }

  public void setRoomType(){
    System.out.println("   1. Training || 2. Meeting || 3. Seminar ");
    System.out.println("**********************************************");
    System.out.print("Enter room type [ex - Training]: ");
    this.RoomType = console.nextLine();
  }

  public void setRoomAddEquip(){
    System.out.println("   1. LCD Projector(RM 100) || 2. Audio Visual Equipment(RM 1,000) ");
    System.out.println("*****************************************************");
    System.out.print("Enter Additional Equipment [ex - 1] *leave blank if no additional equipment* : ");
    this.RoomAddEquip = console.nextLine();
    
  }
  
  public void setRoomOptionMeal(){
    System.out.print("Do you want to meal [Y|N] *1 pax = RM 35 : ");
    String addMeal = console.nextLine();
    if (addMeal.equals("Y")){
      System.out.print("Enter number of pax : ");
      this.RoomOptionMeal = Double.parseDouble(console.nextLine()) * 35.00;
    }
  }

  public void setRoomDuration(){
    System.out.println("  1. Half Day || 2. Full Day || 3. After Office Hour");
    System.out.println("*******************************************************");
    System.out.print("Enter your duration : ");
    this.RoomDuration = Integer.parseInt(console.nextLine());
  }
  
  public void calculateRate(){
    if (this.RoomDuration == 1){
      this.RoomDurationString = "Half Day";
    }else if (this.RoomDuration == 2){
      this.RoomDurationString = "Full Day";
    }else if (this.RoomDuration == 3){
      this.RoomDurationString = "After Office Hour";
    }
    if (this.RoomType.equals("Training")){
      if (this.RoomDuration == 1){
        this.RoomDuration = 500;
      }else if (this.RoomDuration == 2){
        this.RoomDuration = 1000;
      }else if (this.RoomDuration == 3){
        this.RoomDuration = 300;
      }
    }else if(this.RoomType.equals("Seminar")){
      if (this.RoomDuration == 1){
        this.RoomDuration = 1000;
      }else if (this.RoomDuration == 2){
        this.RoomDuration = 2000;
      }else if (this.RoomDuration == 3){
        this.RoomDuration = 750;
      }
    }else if(this.RoomType.equals("Meeting")){
      if (this.RoomDuration == 1){
        this.RoomDuration = 300;
      }else if (this.RoomDuration == 2){
        this.RoomDuration = 600;
      }else if (this.RoomDuration == 3){
        this.RoomDuration = 200;
      }
    }
    this.RoomRate = Double.parseDouble(""+this.RoomDuration);
  }

  public void calculateOther(){
    this.RoomAddRate = 0.00;
    if (this.RoomAddEquip.equals("1")){
      this.RoomAddRate += 100;
    }else if(this.RoomAddEquip.equals("2")){
      this.RoomAddRate += 1000;
    }

    if (this.RoomOptionMeal != 0.00){
      this.RoomAddRate += this.RoomOptionMeal;
    }
  }

  public void calculateTotal(){
    this.totalOverall = this.RoomAddRate + this.RoomDuration + (6/100*(this.RoomAddRate + this.RoomDuration));
  }
  
}